'use strict';

import DB from './db';
import App from './app';

export class Service {

	constructor(options){

		try {

			// init db
			if(options && options.DB && options.APP && options.APP.instances) {

				this.options = options
				this.DB = new DB(this.options.DB);

				if(DB) {
	
					this.App = new App(this.DB,this.options);
					if(this.App){

						if(this.App.argv && this.App.argv.start) {

							this.App.LOG('D','Starting '+this.App.argv.start)
							let idX = this.options.APP.instances.findIndex(x=>x.name === this.App.argv.start);
							if(idX > -1) {

								this.interval(this.options.APP.instances[idX]);
							
							} else {

								this.App.LOG('e',this.App.argv.start + ' not found.')

							}

						} else {

							for(let i=0,c=this.options.APP.instances.length;i<c;i++) {

								this.interval(this.options.APP.instances[i]);

							}

						}
					
					}


				}
	
			} 

		} catch(e) {

			throw e;

		}

	}

	interval(instance) {

		let fn = require(instance.file);

		let that = this;
		that.App.intervals.push(
			{

				name:instance.name,
				interval:setInterval(
					function(){

						that.App.LOG('D',that.App.unixtime()+' - '+instance.name);
						fn(that.App,instance);

					},
					instance.interval
				)

			}
		);

	}

};