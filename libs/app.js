export default class App {

	constructor(DB,IO,options){

		try {

			this.DB = DB;
			this.io = IO;
			this.options = options;
			this.intervals = [];

			this.argv = require('minimist')(process.argv.slice(2));
			this.shortId = require('shortid');

		} catch (e) {

			throw e;
		
		}

	}

	// generate unique stub
	stub(){

		let that = this;
		return new Promise((resolve,reject)=>{

			let shortId = this.shortId.generate();
			this.DB.q(`
					SELECT id 
					FROM \`items\`
					WHERE stub = ?
				`,[shortId])
				.then(verification=>{

					if(verification.length>0){

						that.stub()
							.then(rShortId=>{

								resolve(rShortId);

							})
							.catch(e=>{

								reject(e);

							});

					} else {

						resolve(shortId);

					}

				})
				.catch(e=>{

					reject(e);
				
				});

		});

	}

	unixtime() {

		return Math.floor((new Date()).getTime()/1000);
	
	}

	LOG(type,message,isRaw){

		let logIt = false;
		let typeLevel = 'E';
		let possibleTypes = ['E','D','I'];

		if(!possibleTypes.includes(type)) type = 'E';

		if(this.argv.v){
			logIt = true;
			if(possibleTypes.includes(this.argv.v)) typeLevel = this.argv.v;
		}
		if(process.env._LOGLEVEL) {
			logIt = true;
			if(possibleTypes.includes(process.env._LOGLEVEL)) typeLevel = process.env._LOGLEVEL; 
		}
		let doLog = false;
		if (type === 'E' && logIt && (typeLevel === 'E' || typeLevel === 'D' || typeLevel === 'I')) doLog = true;
		if (type === 'D' && logIt && (typeLevel === 'D' || typeLevel === 'I')) doLog = true;
		if (type === 'I' && logIt && typeLevel === 'I') doLog = true;

		if(doLog){

			if(isRaw) {

				console.log(message);

			} else {

				console.log("\x1b[1m" + type + "\x1b[0m - " + new Date +' - ' + message);
			
			}

		}

		return;

	}

}