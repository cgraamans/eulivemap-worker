module.exports = function(text,DB){

	return new Promise((resolve,reject)=>{

		let re = new RegExp(/[#@]/g, "g");
		let cleanText = text.replace(re,'');
		cleanText = cleanText.replace(/(\r\n\t|\n|\r\t)/gm,' ');
		
		let cleanTextArr = [];
		for(let i=0,c=30;i<c;i++){
			cleanTextArr.push(cleanText);
		}

		let lookupLatLonKeywordCity = function(arrOfKeywords){

			return new Promise((res,rej)=>{

				if(arrOfKeywords.length>0){

					let keywordLookupPromises = [];
					for(let i=0,c=arrOfKeywords.length;i<c;i++){

						if(arrOfKeywords[i].city>0){
						
							keywordLookupPromises.push(new Promise((rs,rj)=>{

								DB.q(`
											SELECT name,lat,lon,countrycode 
											FROM \`data_cities\` 
											WHERE id = ?
										`,
										[arrOfKeywords[i].city]
									)
									.then(cityObj=>{

										if(cityObj.length>0) {

											rs(cityObj[0]);
										
										} else {
										
											rs();
										
										}

									})
									.catch(e=>{

										rj(e);
									
									});
							}));
						
						}
					}
					Promise.all(keywordLookupPromises)
						.then(keywords=>{
							if(keywords) {

								res(keywords);	

							} else {

								res([]);

							}
							
						})
						.catch(e=>{

							rej(e);
						
						});

				} else {

					res([]);

				}

			});

		};

		let concatStrAssembler = function(name,isBinary) {

			let binStr = '';
			if(isBinary) binStr = 'BINARY ';

			return `

				(? LIKE `+binStr+`CONCAT(`+name+`, ' %')) OR
				(? LIKE `+binStr+`CONCAT(`+name+`, ':%')) OR
				(? LIKE `+binStr+`CONCAT(`+name+`, "'%")) OR
				(? LIKE `+binStr+`CONCAT(`+name+`, "-%")) OR
				(? LIKE `+binStr+`CONCAT(`+name+`, '-%')) OR
				(? LIKE `+binStr+`CONCAT(`+name+`, '’%')) OR
				(? LIKE `+binStr+`CONCAT(`+name+`, '"%')) OR
				(? LIKE `+binStr+`CONCAT(`+name+`, '\`%')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, '.')) OR
				(? LIKE `+binStr+`CONCAT('%-', `+name+`, '.')) OR
				(? LIKE `+binStr+`CONCAT('% ',`+name+`)) OR
				(? LIKE `+binStr+`CONCAT('%-',`+name+`)) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, ' %')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, '.%')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, ',%')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, ':%')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, ';%')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, '"%')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, "'%")) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, '\`%')) OR
				(? LIKE `+binStr+`CONCAT('% ', `+name+`, '’%')) OR
				(? LIKE `+binStr+`CONCAT('%-', `+name+`, ' %')) OR
				(? LIKE `+binStr+`CONCAT('%"', `+name+`, ' %')) OR
				(? LIKE `+binStr+`CONCAT("%'", `+name+`, ' %')) OR
				(? LIKE `+binStr+`CONCAT("%’", `+name+`, ' %')) OR
				(? LIKE `+binStr+`CONCAT("%\`", `+name+`, ' %')) OR
				(? LIKE `+binStr+`CONCAT("%\`", `+name+`, '\`%')) OR
				(? LIKE `+binStr+`CONCAT("%’", `+name+`, '’%')) OR
				(? LIKE `+binStr+`CONCAT('%"', `+name+`, '%"')) OR
				(? LIKE `+binStr+`CONCAT("%'", `+name+`, "%'"))

			`;

		};

		let lookupPromises = [

			new Promise((res,rej)=>{

				DB.q(
						`
							SELECT capital as name, countrycode,lat,lon
							FROM \`data_countries\`
							WHERE 
						` + concatStrAssembler('capital',true),
						cleanTextArr
					)
					.then(resultingArr=>{

						res(resultingArr);

					})
					.catch(e=>{

						rej(e);

					});

			}),

			new Promise((res,rej)=>{

				DB.q(
						`
							SELECT name,countrycode,lat,lon
							FROM \`data_countries\`
							WHERE 
						` + concatStrAssembler('name',true),
						cleanTextArr
					)
					.then(resultingArr=>{

						res(resultingArr);

					})
					.catch(e=>{

						rej(e);

					});

			}),

			new Promise((res,rej)=>{

				DB.q(
						`
							SELECT name,countrycode,lat,lon
							FROM \`data_cities\`
							WHERE 
						`+ concatStrAssembler('name',true),
						cleanTextArr
					)
					.then(resultingArr=>{

						res(resultingArr);
					
					})
					.catch(e=>{

						rej(e);
					
					});
			
			}),

			new Promise((res,rej)=>{

				DB.q(
						`	
							SELECT word, city
							FROM \`data_keywords\`
							WHERE 
						`+concatStrAssembler('word'),
						cleanTextArr
					)
					.then(resultingArr=>{

						lookupLatLonKeywordCity(resultingArr)
							.then(latLons=>{

								res(latLons);
							
							})
							.catch(e=>{

								rej(e);

							});

					
					})
					.catch(e=>{

						rej(e);
					
					});
			
			}),

		];

		Promise.all(lookupPromises)
			.then(locationArr=>{

				let locationList = [];
				locationArr.forEach(locationArrayItem=>{

					locationArrayItem.forEach(loc=>{

						let idXName = locationList.findIndex(x=>x.name === loc.name);
						let idXCountryCode = locationList.findIndex(x=>x.countrycode === loc.countrycode);
						if(idXName === -1 || idXCountryCode === -1) locationList.push(loc);

					});						

				});

				resolve(locationList);

			})
			.catch(e=>{

				reject(e);

			});

	});

}