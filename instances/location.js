import twitter from 'twitter';
import shortid from 'shortid';

module.exports = function(App,Options){

	try {

		let nT = ((new Date()).getTime());
		let numRecordsMatching = 0,
			numRecordsTotal = 0;
		let textToLatLon = require('./models/textToLatLon');

		let getLocItemId = function() {

			return new Promise((resolve,reject)=>{

				if(!App.proc) App.proc = {};

				if(!App.proc.Location) {

					App.proc.Location = 0;
					App.DB.q(`SELECT id FROM items ORDER BY id DESC LIMIT 1`,[])
						.then(firstIdArr=>{

							if(firstIdArr.length>0) App.proc.Location = firstIdArr[0].id;
							resolve();

						})
						.catch(e=>{
					
							reject(e);
					
						});


				} else {

					resolve();

				}

			});

		};

		getLocItemId()
			.then(()=>{

				let localId = App.proc.Location;

				App.DB.q(`
							SELECT id, text from items where id > ? ORDER BY id DESC
						`,
						[App.proc.Location]
					)
					.then(procList=>{

						if(procList.length>0) {

							let procListPromises = [];
							procList.forEach(procItem=>{

								numRecordsTotal++;

								procListPromises.push(new Promise((resolve,reject)=>{

									textToLatLon(procItem.text,App.DB)
										.then(latlon=>{
											
											if(latlon.length>0) {

												let latlonPromises = [];
												latlon.forEach(latlonPoint=>{

													latlonPromises.push(new Promise((res,rej)=>{

														let insObj = Object.assign({item_id:procItem.id},latlonPoint);
														App.DB.q(
																'INSERT INTO `item_locations` SET ?',
																insObj
															)
															.then(()=>{

																numRecordsMatching++;
																res();

															})
															.catch(e=>{
																
																rej(e);
															
															});

													}));

												});
												Promise.all(latlonPromises)
													.then(()=>{
													
														resolve();
													
													})
													.catch(e=>{
														
														reject(e);
													
													});


											} else {

												resolve();
											}

										})
										.catch(e=>{
											
											reject(e);
										
										});

								}));

							});

							Promise.all(procListPromises)
								.then(()=>{

									App.proc.Location = procList[0].id;
									App.LOG('D',numRecordsMatching+' records matching locations in '+ (((new Date()).getTime())-nT) +'ms (total:'+numRecordsTotal+')');
									App.LOG('D','^- localId:'+localId+',newId:'+App.proc.Location);

								})
								.catch(e=>{

									console.log(e);

								});

						} else {

							App.LOG('D','0 records for matching locations ('+ (((new Date()).getTime())-nT) +'ms)');
							App.LOG('D','^- localId:'+localId+',newId:'+App.proc.Location);

						}

					})
					.catch(e=>{

						console.log(e);
					
					});

			})
			.catch(e=>{

				console.log(e);
			
			});


	} catch(e) {

		throw e;
	
	}

};