import twitter from 'twitter';
import shortid from 'shortid';

module.exports = function(App,Options){

	try {

		let nT = ((new Date()).getTime());
		let textToLatLon = require('./models/textToLatLon');

		// Get valid sources
		App.DB.q(`
				SELECT s.*, sd.dt 
				FROM \`sources\` AS s 
				LEFT JOIN \`source_dt\` sd ON sd.source_id = s.id
				WHERE 
					(
						(sd.dt < ? AND sd.dt IS NOT NULL) 
						OR 
						sd.dt IS NULL
					) 
					AND s.url_twitter IS NOT NULL 
					AND s.isActive = 1
				ORDER BY rand() 
				LIMIT ?	
			`,
			[App.unixtime() - 300,Options.conf.limit])
			.then(sourceList=>{

				App.LOG('D','sources: '+sourceList.length);
				
				if(sourceList.length>0) {

					// update sources' datetime stamp if available
					let sourcePromises = [];
					for(let i=0,c=sourceList.length;i<c;i++){

						let source = sourceList[i];
						
						sourcePromises.push(new Promise((resolve,reject)=>{
		
							let sourceUpdSQL = `INSERT INTO \`source_dt\` SET ?`;
							let sourceUpdSQLParams = {
								source_id:source.id,
								dt:App.unixtime(),
							};
							if(source.dt) {
								sourceUpdSQL = `UPDATE \`source_dt\` SET dt = ? WHERE source_id = ?;`;
								sourceUpdSQLParams = [App.unixtime(),source.id];
							}
							App.DB.q(sourceUpdSQL,sourceUpdSQLParams)
								.then(()=>{

									resolve();

								})
								.catch(e=>{

									reject(e);

								});

						}));

					}

					Promise.all(sourcePromises)
						.then(()=>{

							// initiate twitter api
							let client = new twitter(Options.conf.twitter);

							// prepare strings
							let finalString = '';
							let processedClientSource = [];

							sourceList.forEach(source=>{
								if(finalString.length<450) {

									let addString = '';
									if(finalString.length>0) {
										addString = '+OR+';		
									}

									// if(!source.url_twitter.startsWith('@')) source.url_twitter = '@'+source.url_twitter;

									if(finalString.length+addString.length+source.url_twitter.length<450){
										finalString+=addString+'from:'+source.url_twitter;
										processedClientSource.push(source);
									}

								}

							});

							client.get('search/tweets',{
									q:finalString,
									tweet_mode:'extended',
									// result_type:'recent',
								},function(error, tweets, response) {

									if(!error) {

										let tweetPromises = [];
										let processedTweetObjects = [];

										let numTweetsProcessed = 0,
											numTweetsRejected = 0;

										// process the tweets
										tweets.statuses.forEach(tweet=>{

											tweetPromises.push(new Promise((resolve,reject)=>{

												// Filter URLs
												let fullText = tweet.full_text.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
												fullText = fullText.trim();

												App.DB.q(`
															SELECT * 
															FROM \`items\` AS i
															INNER JOIN sources s
																ON s.id = i.source_id
															WHERE 
																i.author = ? 
																AND
																i.dt = ?
																AND
																i.text = ?
																AND 
																s.url_twitter = ?
														`,
														[
															tweet.user.screen_name,
															tweet.created_at,
															fullText,
															tweet.user.screen_name
														]
													)
													.then(tweetCheck=>{
														
														if(tweetCheck.length === 0) {

															// unixtime conversions
															let tweetUnixTime = App.unixtime();
															let tweetUnixTimeParse = Date.parse(tweet.created_at);
															if(tweetUnixTimeParse) tweetUnixTime = Math.floor(tweetUnixTimeParse/1000);
															
															// Insert Object
															let insertObj = {

																text:fullText,
																img:tweet.user.profile_image_url_https,
																link:'https://twitter.com/i/web/status/'+tweet.id_str,
																author:tweet.user.screen_name,

																dt:tweet.created_at,
																dt_created:tweetUnixTime,
																dt_unixtime:App.unixtime(),
																
															};

															// verification
															let verificationPromises = [

																// add source and update logo
																new Promise((res,rej)=>{

																	App.DB.q(`
																				SELECT id FROM sources WHERE url_twitter = ?
																			`,
																			[
																				tweet.user.screen_name
																			]
																		)
																		.then(sourceIdArr=>{

																			if(sourceIdArr.length>0){

																				insertObj.source_id = sourceIdArr[0].id;
																				res();

																				// if(sourceIdArr[0].logo !== tweet.user.profile_image_url_https) {

																				// 	App.DB.q(`
																				// 				UPDATE sources SET logo = ? WHERE url_twitter = ?
																				// 			`,
																				// 			[tweet.user.profile_image_url_https,tweet.user.screen_name]
																				// 		)
																				// 		.then(()=>{

																				// 			insertObj.source_id = sourceIdArr[0].id;
																				// 			res();

																				// 		})
																				// 		.catch(e=>{

																				// 			rej(e);

																				// 		});

																				// } else {

																				// 	insertObj.source_id = sourceIdArr[0].id;
																				// 	res();

																				// }

																			} else {

																				rej('missing source from url_twitter');

																			}

																		})
																		.catch(e=>{

																			rej(e);
																		
																		});

																}),

																// add stub
																new Promise((res,rej)=>{

																	App.stub()
																		.then(stub=>{

																			insertObj.stub = stub;
																			res();

																		})
																		.catch(e=>{

																			rej(e);

																		});

																}),

																// add entities
																new Promise((res,rej)=>{

																	// Entity Object Data
																	if(tweet.entities){

																		if(tweet.entities.urls){

																			if(tweet.entities.urls.length>0){

																				insertObj.link = tweet.entities.urls[0].expanded_url;

																			}

																		}

																		if(tweet.entities.media){

																			if(tweet.entities.media.length>0){

																				if (tweet.entities.media[0].media_url_https) {

																					insertObj.img = tweet.entities.media[0].media_url_https;

																				}

																			}

																		}

																	}
																	res();

																}),

															];

															Promise.all(verificationPromises)
																.then(verifiedValues=>{

																	processedTweetObjects.push(insertObj);

																	resolve();

																})
																.catch(e=>{

																	reject(e);

																});

														} else {

															numTweetsRejected++;
															resolve();

														}
														
													})
													.catch(e=>{

														reject(e);
													
													});

											})); // promise pushes end

										}); // end of foreach

										Promise.all(tweetPromises)
											.then(()=>{

												if(processedTweetObjects.length>0) {

													// bulk insert
													let processedTweetArray = [];
													processedTweetObjects.forEach(to=>{

														processedTweetArray.push([
															to.source_id,
															to.text,
															to.stub,
															to.link,
															to.author,
															to.img,
															to.dt,
															to.dt_created,
															to.dt_unixtime
														]);
														numTweetsProcessed++;

													});

													App.DB.q(
															`
																INSERT INTO \`items\` 
																(source_id,text,stub,link,author,img,dt,dt_created,dt_unixtime) 
																VALUES ?
															`,
															[processedTweetArray]
														)
														.then(()=>{

															App.LOG('D',numTweetsProcessed+' tweets processed in '+ (((new Date()).getTime())-nT) +'ms ('+numTweetsRejected+' rejected)');

														})
														.catch(e=>{

															console.log(e);
														
														});

												} else {

													App.LOG('D','0 tweets processed in '+ (((new Date()).getTime())-nT) +'ms ('+numTweetsRejected+' rejected)');

												}

											})
											.catch(e=>{

												console.log(e);
											
											});

									} else {

										console.log(error);
										console.log(response);  // Raw response object.
									
									}

								});	// client.get				

						})
						.catch(e=>{

							console.log(e);

						});

				}

			})
			.catch(e=>{

				console.log('e');

			})

	} catch(e) {

		throw e;
	
	}

};