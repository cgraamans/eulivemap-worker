// import twitter from 'twitter';
import shortid from 'shortid';

module.exports = function(App,Options){

	try {

		let Parser = require('rss-parser');
		let parser = new Parser();

		let nT = ((new Date()).getTime());;

		// Get valid sources
		App.DB.q(`
				SELECT s.*, sd.dt 
				FROM \`sources\` AS s 
				LEFT JOIN \`source_dt\` sd ON sd.source_id = s.id
				WHERE 
					(
						(sd.dt < ? AND sd.dt IS NOT NULL) 
						OR 
						sd.dt IS NULL
					) 
					AND s.url_rss IS NOT NULL 
					AND s.isActive = 1
				ORDER BY sd.dt ASC
				LIMIT ?	
			`,
			[App.unixtime() - 300,Options.conf.limit])
			.then(sourceList=>{
				
				App.LOG('D','sources: '+sourceList.length);

				if(sourceList.length>0) {

					// update sources' datetime stamp if available
					let sourcePromises = [];
					for(let i=0,c=sourceList.length;i<c;i++){

						let source = sourceList[i];
						
						sourcePromises.push(new Promise((resolve,reject)=>{
		
							let sourceUpdSQL = `INSERT INTO \`source_dt\` SET ?`;
							let sourceUpdSQLParams = {
								source_id:source.id,
								dt:App.unixtime(),
							};
							if(source.dt) {
								sourceUpdSQL = `UPDATE \`source_dt\` SET dt = ? WHERE source_id = ?;`;
								sourceUpdSQLParams = [App.unixtime(),source.id];
							}
							App.DB.q(sourceUpdSQL,sourceUpdSQLParams)
								.then(()=>{

									resolve();

								})
								.catch(e=>{

									reject(e);

								});

						}));

					}

					Promise.all(sourcePromises)
						.then(()=>{

							// variables for promise execution
							let parseUrlPromises = [];
							let itemReadyForIns = [];
							let numDuplicateItems = 0;
							
							sourceList.forEach(source=>{

								parseUrlPromises.push(new Promise((resolve,reject)=>{

									setTimeout(function(){

										// parse the url with rss-parser
										parser.parseURL(source.url_rss).then((feed) => {

											let itemCheckPromise = [];
											if(feed && feed.items.length > 0) {

												feed.items.forEach(item=>{

													itemCheckPromise.push(new Promise((res,rej)=>{

														item.title = item.title.trim();

														if(item.title.length > 0) {


															// rssCheck to see if the item already exists
															App.DB.q(`
																	SELECT * 
																	FROM \`items\` AS i
																	INNER JOIN sources s
																		ON s.id = i.source_id
																	WHERE 
																		i.link = ?
																		AND
																		i.text = ?
																		AND 
																		s.id = ?
																`,
																[
																	item.link,
																	item.title,
																	source.id
																]
															)
															.then(rssCheck=>{

																if(rssCheck.length > 0) {

																	numDuplicateItems++;
																	res();
																
																} else {

																	App.stub()
																		.then(stub=>{

																			// unixtime conversions
																			let UnixTime = App.unixtime();
																			let UnixTimeParse = Date.parse(item.pubDate);
																			if(UnixTimeParse) UnixTime = Math.floor(UnixTimeParse/1000);
																			if(UnixTime > App.unixtime()) {
																				UnixTime = App.unixtime();
																				item.pubDate = new Date();
																			}

																			// insert item preparation
																			let insItem = [
																				source.id,
																				item.title,
																				stub,
																				item.link,
																				(item.author && typeof item.author === 'string' ? item.author : source.name),
																				(item.content ? item.content : null),
																				(item.contentSnippet ? item.contentSnippet : null),
																				(item.pubDate? item.pubDate : (new Date())),
																				UnixTime,
																				App.unixtime()
																			];

																			// add to insert array
																			itemReadyForIns.push(insItem);
																			res();

																		})
																		.catch(e=>{
																			rej(e);
																		});
																	
																}
															})
															.catch(e=>{
																rej(e)

															});


														} else {

															res();

														}


													}));

													Promise.all(itemCheckPromise)
														.then(()=>{
															resolve();
														})
														.catch(e=>{
															reject(e);
														})

												});

											} else {

												resolve();
											
											}

										}).catch((err) => {
											App.LOG('E','Error parsing ' + source.name + ' (url: '+source.url_rss+')');
											App.LOG('E',err,true);
											// App.LOG('E',err,true);										
											resolve();
										});

									},
									Math.random()*Options.conf.jitter);


								}));

							});

							Promise.all(parseUrlPromises)
								.then(()=>{

									if(itemReadyForIns.length > 0) {

										App.DB.q(
												`
													INSERT INTO \`items\` 
													(source_id,text,stub,link,author,description,summary,dt,dt_created,dt_unixtime) 
													VALUES ?
												`,
												[itemReadyForIns]
											)
											.then(()=>{

												App.LOG('D',itemReadyForIns.length+' rss items processed in '+ (((new Date()).getTime())-nT) +'ms ('+numDuplicateItems+' rejected)');

											})
											.catch(e=>{

												App.LOG('E',e,true);
											
											});

									} else {

										App.LOG('D','0 rss items processed in '+ (((new Date()).getTime())-nT) +'ms ('+numDuplicateItems+' rejected)');

									}


								})
								.catch(e=>{

									App.LOG('E',e,true);
								
								});			

						})
						.catch(e=>{

							App.LOG('E',e,true);

						});

				}

			})
			.catch(e=>{

				App.LOG('E',e,true);

			})

	} catch(e) {

		throw e;
	
	}

};