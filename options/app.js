const APP = {

	instances:[

		{
			name:'twitter',
			file:'../instances/twitter.js',
			interval: 60000, //ms
			conf:{
				limit:10,
				twitter:{
					consumer_key: process.env.EUSPIDER_TW_C_KEY,
					consumer_secret: process.env.EUSPIDER_TW_C_SECRET,
					access_token_key: process.env.EUSPIDER_TW_AT_KEY,
					access_token_secret: process.env.EUSPIDER_TW_AT_SECRET,
				}
			},
		},
		{
			name:'locations',
			file:'../instances/location.js',
			interval: 20000, //ms
		},
		{
			name:'rss',
			file:'../instances/rss.js',
			interval: 60000, //ms
			conf:{
				jitter: 1000, // scale time for each rss fetch to reduce server load
				limit:5, // feeds at once
			}
		},

	]

};
export default APP;